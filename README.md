# Guía de hoteles


### Descargar el proyecto

###### Via HTTPS:
```bash
	$ git clone https://gitlab.com/cabreradavid/guia-hoteles.git 
```


### Descargar dependencias del proyecto

```bash
	$ npm install --save-dev
```


### Ejecutar el proyecto

```bash
	$ npm run dev
```