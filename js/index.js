$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal se está mostrando');
        $('#contactoBtn').removeClass('btn-outline-danger');
        $('#contactoBtn').addClass('btn-primary');

    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal se cerró');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-danger');
    });
});
